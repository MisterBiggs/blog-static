import os

import glob


widget = """
<script data-name="BMC-Widget" src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js" data-id="Anson" data-description="Support my work on Buy me a coffee!" data-message="Please consider donating if you enjoy my content!" data-color="#5F7FFF" data-position="right" data-x_margin="18" data-y_margin="18"></script>
</head>
"""
# Add widget to all posts
for filename in glob.glob("*/index.html", recursive=True):
    print(filename)
    with open(filename, "r+", encoding="utf-8") as f:
        txt = f.read().replace("</head>", widget)
        f.seek(0)
        f.writelines(txt)
# print(f.read())

# Add widget to home page
with open("index.html", "r+", encoding="utf-8") as f:
    txt = f.read().replace("</head>", widget)
    f.seek(0)
    f.writelines(txt)
#
